#!/bin/bash

# Default Config options
declare -A config=(
    [reinstall]=false
    [target]="/dev/sda"
    [rootmnt]="/mnt/arch_system"
    [locale]="en_US.UTF-8"
    [locale_full]="en_US.UTF-8 UTF-8"
    [keymap]="us"
    [country]="Ukraine"
    [timezone]="Europe/Kiev"
    [hostname]="legion"
    [username]="antrix"
    [username_full]="Antrix de Tylmarande"
    [password]="\$6\$m.IkwJKGSNtgxhnO\$XkMDzZ71zOk5k6XwgIVdASw.ahDIShzGwjPT5nL/RROjPsArVowGbUanWalC0tKeN1GzjYYR/Rsckkpt.XbRw/" # mkpasswd -m sha-512
    [desktop]="gnome"
    [mirrors_update]=false
    [pacman_noconfirm]=true
    [pacman_needed]=true
    [pacman_disable_dl_timeout]=true
    [pacman_ask]=true
    [stage_clear]=false
)

print_help() {
    cat << EOF
Usage: $0 [options]

Options:
  --reinstall=[true|false]              Reinstall mode (default: ${config[reinstall]})
  --target=DEVICE                       Target device (default: ${config[target]})
  --rootmnt=PATH                        Root mount directory (default: ${config[rootmnt]})
  --locale=LOCALE                       Locale setting for /etc/locale.conf (default: ${config[locale_short]})
  --locale_full=LOCALE_FULL             Locale setting for /etc/locale.gen (default: ${config[locale_full]})
  --keymap=KEYMAP                       Keyboard layout (default: ${config[keymap]})
  --country=COUNTRY                     Country setting (default: ${config[country]})
  --timezone=TIMEZONE                   Timezone setting (default: ${config[timezone]})
  --hostname=HOSTNAME                   Hostname (default: ${config[hostname]})
  --username=USERNAME                   Username (default: ${config[username]})
  --username_full="FULL NAME"           Full name of the user (default: ${config[username_full]})
  --password=PASSWORD                   User password (default: encrypted)
  --desktop=DESKTOP                     Desktop environment (default: ${config[desktop]})
  --mirrors_update=[true|false]         Update mirrors (default: ${config[mirrors_update]})
  --pacman_noconfirm=[true|false]       Pacman no confirm (default: ${config[pacman_noconfirm]})
  --pacman_needed=[true|false]          Pacman needed (default: ${config[pacman_needed]})
  --pacman_disable_dl_timeout=[true|false] Disable pacman download timeout (default: ${config[pacman_disable_dl_timeout]})
  --pacman_ask=[true|false]             Ask for pacman options (default: ${config[pacman_ask]})
  --stage_clear=[true|false]            Clear stage logs (default: ${config[stage_clear]})
  --help                                Display this help message
EOF
}

# Parse command line arguments
for arg in "$@"; do
    case $arg in
        --help)
            print_help
            exit 0
            ;;
        --*=*)
            key="${arg%%=*}"
            value="${arg#*=}"
            key="${key#--}"
            if [[ -n "${config[$key]+_}" ]]; then
                config[$key]="$value"
            else
                echo "Unknown option: $arg"
                print_help
                exit 1
            fi
            ;;
        *)
            echo "Unknown option: $arg"
            print_help
            exit 1
            ;;
    esac
done