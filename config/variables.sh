#!/bin/bash

# Storage Serial Numbers
storage_blacklisted=("S462NF0M317264R") # Samsung

# VM type (we will check if the system is in a VM and its type)
vm_type="none"

# Arrays for services
services_to_enable=()
services_to_enable_user=()
services_to_mask=()

# Arrays for kernel parameters
kernel_params=()

# Arrays for mkinitcpio parameters
mkinitcpio_binaries=()
mkinitcpio_hooks=()
mkinitcpio_modules=()