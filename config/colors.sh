#!/bin/bash

# Basic color variables
BLACK='\033[0;30m'                      # Black text
RED='\033[0;31m'                        # Red text
GREEN='\033[0;32m'                      # Green text
YELLOW='\033[0;33m'                     # Yellow text
BLUE='\033[0;34m'                       # Blue text
MAGENTA='\033[0;35m'                    # Magenta text
CYAN='\033[0;36m'                       # Cyan text
WHITE='\033[1;37m'                      # White text
GRAY='\033[0;37m'                       # Gray text

# Bright color variables
BRIGHT_BLACK='\033[1;30m'               # Bright Black (Dark Gray) text
BRIGHT_RED='\033[1;31m'                 # Bright Red text
BRIGHT_GREEN='\033[1;32m'               # Bright Green text
BRIGHT_YELLOW='\033[1;33m'              # Bright Yellow text
BRIGHT_BLUE='\033[1;34m'                # Bright Blue text
BRIGHT_MAGENTA='\033[1;35m'             # Bright Magenta text
BRIGHT_CYAN='\033[1;36m'                # Bright Cyan text
BRIGHT_WHITE='\033[1;37m'               # Bright White text

# Background color variables
BACKGROUND_BLACK='\033[40m'             # Black background
BACKGROUND_RED='\033[41m'               # Red background
BACKGROUND_GREEN='\033[42m'             # Green background
BACKGROUND_YELLOW='\033[43m'            # Yellow background
BACKGROUND_BLUE='\033[44m'              # Blue background
BACKGROUND_MAGENTA='\033[45m'           # Magenta background
BACKGROUND_CYAN='\033[46m'              # Cyan background
BACKGROUND_WHITE='\033[47m'             # White background
BACKGROUND_GRAY='\033[100m'             # Gray background

BACKGROUND_BRIGHT_RED='\033[101m'       # Bright Red background
BACKGROUND_BRIGHT_GREEN='\033[102m'     # Bright Green background
BACKGROUND_BRIGHT_YELLOW='\033[103m'    # Bright Yellow background
BACKGROUND_BRIGHT_BLUE='\033[104m'      # Bright Blue background
BACKGROUND_BRIGHT_MAGENTA='\033[105m'   # Bright Magenta background
BACKGROUND_BRIGHT_CYAN='\033[106m'      # Bright Cyan background
BACKGROUND_BRIGHT_WHITE='\033[107m'     # Bright White background

# Foreground color variables
FOREGROUND_BLACK='\033[30m'             # Black text
FOREGROUND_RED='\033[31m'               # Red text
FOREGROUND_GREEN='\033[32m'             # Green text
FOREGROUND_YELLOW='\033[33m'            # Yellow text
FOREGROUND_BLUE='\033[34m'              # Blue text
FOREGROUND_MAGENTA='\033[35m'           # Magenta text
FOREGROUND_CYAN='\033[36m'              # Cyan text
FOREGROUND_WHITE='\033[37m'             # White text
FOREGROUND_GRAY='\033[90m'              # Gray text
FOREGROUND_BRIGHT_RED='\033[91m'        # Bright Red text
FOREGROUND_BRIGHT_GREEN='\033[92m'      # Bright Green text
FOREGROUND_BRIGHT_YELLOW='\033[93m'     # Bright Yellow text
FOREGROUND_BRIGHT_BLUE='\033[94m'       # Bright Blue text
FOREGROUND_BRIGHT_MAGENTA='\033[95m'    # Bright Magenta text
FOREGROUND_BRIGHT_CYAN='\033[96m'       # Bright Cyan text
FOREGROUND_BRIGHT_WHITE='\033[97m'      # Bright White text

# No Color
NC='\033[0m' 