#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: Open Drivers ]

# Installing Open drivers
log_info "Installing Open drivers..."
pacman_chroot "${pkgs_drivers_open[@]}" || { log_error "Failed to install VM drivers packages."; exit 1; }