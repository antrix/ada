#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: Intel Drivers ]

# Installing Intel drivers
log_info "Installing Intel drivers..."
pacman_chroot "${pkgs_drivers_intel[@]}" || { log_error "Failed to install Intel drivers packages."; exit 1; }