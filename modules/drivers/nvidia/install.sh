#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: Nvidia Drivers ]

# Installing NVIDIA drivers
log_info "Installing NVIDIA drivers..."
pacman_chroot "${pkgs_drivers_nvidia[@]}" || { log_error "Failed to install NVIDIA drivers packages."; exit 1; }

# Add NVIDIA driver options to /etc/modprobe.d/nvidia.conf
log_info "Configuring /etc/modprobe.d/nvidia.conf..."
arch-chroot "${config[rootmnt]}" bash -c "cat > /etc/modprobe.d/nvidia.conf << EOF
options nvidia NVreg_PreserveVideoMemoryAllocations=1 NVreg_TemporaryFilePath=/var/tmp
options nvidia-drm modeset=1
options nvidia-drm fbdev=1
options nvidia NVreg_EnableGpuFirmware=0
EOF" || { log_error "Failed to create nvidia.conf."; exit 1; }
log_debug "Created /etc/modprobe.d/nvidia.conf with NVIDIA driver options"

# Add NVIDIA modules to mkinitcpio.conf
log_info "Configuring /etc/mkinitcpio.conf..."
arch-chroot "${config[rootmnt]}" bash -c "sed -i 's/^MODULES=.*/MODULES=(nvidia nvidia_modeset nvidia_uvm nvidia_drm)/' /etc/mkinitcpio.conf"
log_debug "Added NVIDIA driver modules to initramfs configuration"

# Add NVIDIA services to services_to_enable
services_to_enable+=("nvidia-hibernate.service" "nvidia-persistenced.service" "nvidia-powerd.service" "nvidia-resume.service" "nvidia-suspend.service")
log_debug "Added to services_to_enable: nvidia-hibernate.service, nvidia-persistenced.service, nvidia-powerd.service, nvidia-resume.service, nvidia-suspend.service"
