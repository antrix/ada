#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment

# Importing drivers packages
source packages/drivers.sh


### [ Section: Drivers ]

# Drivers Installation stage log
log_stage "Drivers Installation"

if [ "$vm_type" = "none" ]; then
    # Install Intel drivers
    source modules/drivers/intel/install.sh

    # Install NVIDIA drivers
    source modules/drivers/nvidia/install.sh

else
    log_info "Virtual machine detected: $vm_type. Installing VM drivers and tools..."

    # Install Open drivers
    source modules/drivers/open/install.sh

    # Install VM drivers
    source modules/drivers/vm/install.sh
fi
