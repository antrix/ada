#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: VM Drivers ]

# Installing VM drivers
log_info "Installing VM drivers..."
case $vm_type in
    "vmware")
        pacman_chroot open-vm-tools || { log_error "Failed to install VMware tools."; exit 1; }
        services_to_enable+=("vmtoolsd" "vmware-vmblock-fuse")
        log_debug "Added to services_to_enable: vmtoolsd, vmware-vmblock-fuse"
        ;;
    "virtualbox")
        pacman_chroot virtualbox-guest-utils || { log_error "Failed to install VirtualBox guest utils."; exit 1; }
        services_to_enable+=("vboxservice")
        log_debug "Added to services_to_enable: vboxservice"
        ;;
    "qemu" | "kvm")
        pacman_chroot spice spice-vdagent spice-protocol spice-gtk qemu-guest-agent || { log_error "Failed to install QEMU guest agent."; exit 1; }
        services_to_enable+=("qemu-guest-agent")
        log_debug "Added to services_to_enable: qemu-guest-agent"
        ;;
    "hyperv")
        pacman_chroot hyperv || { log_error "Failed to install Hyper-V tools."; exit 1; }
        services_to_enable+=("hv_fcopy_daemon" "hv_kvp_daemon" "hv_vss_daemon")
        log_debug "Added to services_to_enable: hv_fcopy_daemon, hv_kvp_daemon, hv_vss_daemon"
        ;;
    *)
        log_warning "Unknown VM type detected: $vm_type. No specific guest tools installed."
        ;;
esac
