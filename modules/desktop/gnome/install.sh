#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: GNOME ]

# Installing GNOME Desktop Environment
log_info "Installing GNOME"
pacman_chroot "${pkgs_gnome[@]}" || { log_error "Failed to install GNOME desktop environment."; exit 1; }

# Installing and enabling services
services_to_enable+=("gdm" "bluetooth")
log_debug "Added to services_to_enable: gdm, bluetooth"

# Installing and enabling plymouth's parameters
mkinitcpio_hooks+=("plymouth")
log_debug "Added to mkinitcpio_hooks: plymouth"

kernel_params+=("splash")
log_debug "Added to kernel_params: splash"

# Creating symlink to disable udev rules for GDM
log_info "Disabling udev rules for GDM"
arch-chroot ${config[rootmnt]} bash -c "ln -sf /dev/null /etc/udev/rules.d/61-gdm.rules" || { log_error "Failed to create symlink for 61-gdm.rules."; exit 1; }
log_debug "Created symlink to /dev/null for 61-gdm.rules"

# Firefox LibAdwaita theme configuration
source modules/desktop/gnome/config_firefox_theme.sh