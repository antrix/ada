#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: Firefox Theme Installation ]

install_theme=true

# Check if Firefox is installed
if ! arch-chroot ${config[rootmnt]} sudo -u ${config[username]} pacman -Q firefox &> /dev/null; then
    log_info "Firefox is not installed. Skipping theme installation."
    install_theme=false
fi

# Check if the firefox-gnome-theme is installed
if ! arch-chroot ${config[rootmnt]} sudo -u ${config[username]} pacman -Q firefox-gnome-theme &> /dev/null; then
    log_info "firefox-gnome-theme is not installed. Skipping theme installation."
    install_theme=false
fi

# Check if the desktop environment is GNOME
if [ "${config[desktop]}" != "gnome" ]; then
    log_info "Skipping Firefox LibAdwaita theme installation as the desktop environment is not GNOME."
    install_theme=false
fi

# Installing
if [ "$install_theme" = true ]; then
    log_info "Installing firefox-gnome-theme..."

    # Define directories and files
    profile_dir="/home/${config[username]}/.mozilla/firefox/default-release"
    profile_ini_path="/home/${config[username]}/.mozilla/firefox/profiles.ini"
    installs_ini_path="/home/${config[username]}/.mozilla/firefox/installs.ini"
    chrome_dir="$profile_dir/chrome"

    # Create all necessary directories
    arch-chroot ${config[rootmnt]} sudo -u ${config[username]} mkdir -p "$profile_dir" "$chrome_dir" || { log_error "Failed to create necessary directories."; install_theme=false; }
    log_debug "Necessary directories created or already exist."

    # Create profiles.ini file
    arch-chroot ${config[rootmnt]} sudo -u ${config[username]} bash -c "cat > $profile_ini_path << EOF
[Install4F96D1932A9F858E]
Default=default-release
Locked=1

[Profile0]
Name=default-release
IsRelative=1
Path=default-release
Default=1

[General]
StartWithLastProfile=1
Version=2
EOF" || { log_error "Failed to create profiles.ini."; install_theme=false; }
    log_debug "profiles.ini created."

    # Create installs.ini file
    arch-chroot ${config[rootmnt]} sudo -u ${config[username]} bash -c "cat > $installs_ini_path << EOF
[4F96D1932A9F858E]
Default=default-release
Locked=1
EOF" || { log_error "Failed to create installs.ini."; install_theme=false; }
    log_debug "installs.ini created."

    # Create symlink for the theme
    arch-chroot ${config[rootmnt]} sudo -u ${config[username]} ln -sf /usr/lib/firefox-gnome-theme "$chrome_dir/firefox-gnome-theme" || { log_error "Failed to create symlink for firefox-gnome-theme."; install_theme=false; }
    log_debug "Symlink for firefox-gnome-theme created."

    # Create or update userChrome.css
    arch-chroot ${config[rootmnt]} sudo -u ${config[username]} bash -c "echo '@import \"firefox-gnome-theme/userChrome.css\";' > $chrome_dir/userChrome.css" || { log_error "Failed to update userChrome.css."; install_theme=false; }
    log_debug "userChrome.css created or updated."

    # Create or update userContent.css
    arch-chroot ${config[rootmnt]} sudo -u ${config[username]} bash -c "echo '@import \"firefox-gnome-theme/userContent.css\";' > $chrome_dir/userContent.css" || { log_error "Failed to update userContent.css."; install_theme=false; }
    log_debug "userContent.css created or updated."

    # Create symlink for user.js
    arch-chroot ${config[rootmnt]} sudo -u ${config[username]} ln -sf "$chrome_dir/firefox-gnome-theme/configuration/user.js" "$profile_dir/user.js" || { log_error "Failed to create symlink for user.js."; install_theme=false; }
    log_debug "Symlink for user.js created."
fi