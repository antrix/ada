#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment

# Import GNOME packages
source packages/gnome.sh

# Import KDE Plasma 6 packages
source packages/plasma.sh

# Import Hyprland packages
source packages/hyprland.sh

### [ Section: Desktop Environment and Services ]

# Desktop Environment installation stage log
log_stage "Desktop Environment Installation"

# Desktop Environment installation
case ${config[desktop]} in
    # "cinnamon")
    #     source modules/desktop/cinnamon/install.sh
    #     ;;
    "gnome")
        source modules/desktop/gnome/install.sh
        ;;
    "hyprland")
        source modules/desktop/hyprland/install.sh
        ;;
    # "lxqt")
    #     source modules/desktop/lxqt/install.sh
    #     ;;
    # "mate")
    #     source modules/desktop/mate/install.sh
    #     ;;
    "plasma")
        source modules/desktop/plasma/install.sh
        ;;
    # "xfce")
    #     source modules/desktop/xfce/install.sh
    #     ;;
    *)
        log_error "Unsupported desktop environment: ${config[desktop]}"
        exit 1
        ;;
esac