#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: KDE Plasma ]

# Installing KDE Plasma Desktop Environment
log_info "Installing KDE Plasma"
pacman_chroot "${pkgs_plasma[@]}" || { log_error "Failed to install KDE Plasma desktop environment."; exit 1; }

# Installing and enabling services
services_to_enable+=("sddm" "bluetooth")
log_debug "Added to services_to_enable: sddm, bluetooth"