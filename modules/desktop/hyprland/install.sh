#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: Hyprland ]

# Installing Hyprland Desktop Environment
log_info "Installing Hyprland"
pacman_chroot "${pkgs_hyprland[@]}" || { log_error "Failed to install Hyprland desktop environment."; exit 1; }

# Installing and enabling services
services_to_enable+=("sddm")
log_debug "Added to services_to_enable: sddm"
