#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment

# Import Apps packages
source packages/apps.sh


### [ Section: Extras ]

# Extras installation stage log
log_stage "Extras Installation"

# Extras applications installation
source modules/extras/apps/install.sh
