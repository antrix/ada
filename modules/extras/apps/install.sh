#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: Applications ]

# Installing applications packages
log_info "Installing applications packages..."
pacman_chroot "${pkgs_apps[@]}" || { log_error "Failed to install applications packages."; exit 1; }
