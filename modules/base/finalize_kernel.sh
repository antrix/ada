#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: Kernel Parameters ]

# Adding kernel parameters
log_info "Adding kernel parameters..."
add_kernel_params "${kernel_params[@]}" || { log_error "Failed to add kernel parameters."; exit 1; }
log_debug "Added kernel parameters"
