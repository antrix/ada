#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment

# Importing display servers packages
source packages/power.sh


### [ Section: Power and Thermal Management ]

# Power/Thermal stage log 
log_stage "Power/Thermal installation and configuration"

# Installing power and thermal management packages
log_info "Installing power and thermal management packages..."
pacman_chroot "${pkgs_power_thermal[@]}" || { log_error "Failed to install thermal management packages."; exit 1; }
log_debug "Installed power and thermal management packages"

# Installing and enabling services
services_to_enable+=("power-profiles-daemon.service" "lm_sensors.service" "thermald.service")
log_debug "Added services to enable: power-profiles-daemon.service, lm_sensors.service, thermald.service"
