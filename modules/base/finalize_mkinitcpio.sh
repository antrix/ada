#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: mkinitcpio Parameters ]

# Adding mkinitcpio parameters
log_info "Adding mkinitcpio hooks..."
add_mkinitcpio_hooks "${mkinitcpio_hooks[@]}"
log_debug "Added mkinitcpio hooks"

log_info "Adding mkinitcpio modules..."
add_mkinitcpio_modules "${mkinitcpio_modules[@]}"
log_debug "Added mkinitcpio modules"

log_info "Adding mkinitcpio binaries..."
add_mkinitcpio_binaries "${mkinitcpio_binaries[@]}"
log_debug "Added mkinitcpio binaries"

### [ Section: Initramfs Generation ]

# Generating initramfs
log_info "Generating initramfs..."
arch-chroot ${config[rootmnt]} mkinitcpio -P || { log_error "Failed to generate initramfs."; exit 1; }