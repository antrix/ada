#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: Cleanup Deployment ]

# Cleaning up installed system
log_info "Cleaning up installed system..."
arch-chroot ${config[rootmnt]} pacman -Scc --noconfirm || { log_error "Failed to clear pacman cache in installed system."; exit 1; }
log_debug "Cleared pacman cache in installed system"

arch-chroot ${config[rootmnt]} rm -rf /tmp/* || { log_error "Failed to remove temporary files in installed system."; exit 1; }
log_debug "Removed temporary files in installed system"

arch-chroot ${config[rootmnt]} rm -rf /var/cache/pacman/pkg/* || { log_error "Failed to remove downloaded packages in installed system."; exit 1; }
log_debug "Removed downloaded packages in installed system"

log_info "Installation completed!"
sync
log_debug "Filesystem buffers synchronized"
