#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: mkinitcpio ]

# Configuring mkinitcpio.conf
log_info "Configuring mkinitcpio.conf..."
arch-chroot ${config[rootmnt]} sed -i 's/^BINARIES=.*/BINARIES=(\/usr\/bin\/btrfs)/' /etc/mkinitcpio.conf || { log_error "Failed to configure mkinitcpio.conf BINARIES."; exit 1; }
current_binaries=$(arch-chroot ${config[rootmnt]} grep '^BINARIES=' /etc/mkinitcpio.conf)
log_debug "Configured mkinitcpio.conf BINARIES: $current_binaries"

arch-chroot ${config[rootmnt]} sed -i 's/^MODULES=.*/MODULES=(btrfs)/' /etc/mkinitcpio.conf || { log_error "Failed to configure mkinitcpio.conf MODULES."; exit 1; }
current_modules=$(arch-chroot ${config[rootmnt]} grep '^MODULES=' /etc/mkinitcpio.conf)
log_debug "Configured mkinitcpio.conf MODULES: $current_modules"

arch-chroot ${config[rootmnt]} sed -i 's/^HOOKS=.*/HOOKS=(base udev autodetect microcode keyboard keymap modconf block filesystems fsck)/' /etc/mkinitcpio.conf || { log_error "Failed to configure mkinitcpio.conf HOOKS."; exit 1; }
current_hooks=$(arch-chroot ${config[rootmnt]} grep '^HOOKS=' /etc/mkinitcpio.conf)
log_debug "Configured mkinitcpio.conf HOOKS: $current_hooks"
