#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: Timezone & Time ]

# Setting timezone for the installed system
log_info "Setting timezone..."
arch-chroot ${config[rootmnt]} ln -sf /usr/share/zoneinfo/${config[timezone]} /etc/localtime || { log_error "Failed to set timezone."; exit 1; }
log_debug "Timezone set to ${config[timezone]}"

arch-chroot ${config[rootmnt]} hwclock --systohc || { log_error "Failed to synchronize hardware clock."; exit 1; }
current_time=$(arch-chroot ${config[rootmnt]} date)
log_debug "Hardware clock synchronized: $current_time"


### [ Section: Pacman ]

# Customizing pacman
log_info "Configuring pacman..."
arch-chroot ${config[rootmnt]} sed -i 's/^#Color$/Color/' /etc/pacman.conf || { log_error "Failed to enable pacman color."; exit 1; }
log_debug "Pacman color enabled"

arch-chroot ${config[rootmnt]} sed -i 's/^#ParallelDownloads = 5/ParallelDownloads = 50/' /etc/pacman.conf || { log_error "Failed to configure pacman parallel downloads."; exit 1; }
log_debug "Pacman parallel downloads set to 50"


### [ Section: Repositories ]

# Enabling multilib repository
log_info "Enabling multilib repository..."
arch-chroot ${config[rootmnt]} sed -i '/^#\[multilib\]$/,/^#Include/ s/^#//' /etc/pacman.conf || { log_error "Failed to enable multilib repository."; exit 1; }
log_debug "Multilib repository enabled"

# Adding A.L.I.C.E. repository
log_info "Adding A.L.I.C.E. repository..."
if ! arch-chroot ${config[rootmnt]} grep -q "\[alice\]" /etc/pacman.d/alice.conf 2>/dev/null; then
    echo -e "[alice]\nSigLevel = Optional TrustAll\nServer = http://94.16.114.182" | arch-chroot ${config[rootmnt]} tee /etc/pacman.d/alice.conf > /dev/null || { log_error "Failed to create /etc/pacman.d/alice.conf."; exit 1; }
    log_debug "A.L.I.C.E. repository configuration file created"
fi

# Adding include for A.L.I.C.E. repository in pacman.conf
if ! arch-chroot ${config[rootmnt]} grep -q "Include = /etc/pacman.d/alice.conf" /etc/pacman.conf 2>/dev/null; then
    echo -e "\nInclude = /etc/pacman.d/alice.conf" | arch-chroot ${config[rootmnt]} tee -a /etc/pacman.conf > /dev/null || { log_error "Failed to add include for A.L.I.C.E. repository to pacman.conf."; exit 1; }
    log_debug "Include for A.L.I.C.E. repository added to pacman.conf"
fi

# Update package database after enabling multilib
log_info "Updating package database..."
arch-chroot ${config[rootmnt]} pacman -Syy || { log_error "Failed to update package database."; exit 1; }
log_debug "Package database updated"


### [ Section: Environment ]

# Setting up environment
log_info "Setting up environment..."
arch-chroot ${config[rootmnt]} sed -i "s/^#${config[locale_full]}/${config[locale_full]}/" /etc/locale.gen || { log_error "Failed to configure locale."; exit 1; }
log_debug "Locale configured in /etc/locale.gen: ${config[locale_full]}"

arch-chroot ${config[rootmnt]} locale-gen || { log_error "Failed to generate locale."; exit 1; }
log_debug "Locale generated"

echo "LANG=${config[locale]%% *}" > ${config[rootmnt]}/etc/locale.conf || { log_error "Failed to set locale.conf."; exit 1; }
log_debug "Locale set in /etc/locale.conf: LANG=${config[locale]%% *}"

# Setting console keymap
log_info "Setting console keymap..."
echo "KEYMAP=${config[keymap]}" > ${config[rootmnt]}/etc/vconsole.conf || { log_error "Failed to set console keymap."; exit 1; }
log_debug "Console keymap set to ${config[keymap]}"


### [ Section: fstab ]

# Generate an fstab file based on the mounted file systems
log_info "Generating fstab..."
genfstab -L -p ${config[rootmnt]} >> ${config[rootmnt]}/etc/fstab || { log_error "Failed to configure fstab."; exit 1; }
log_debug "fstab generated at ${config[rootmnt]}/etc/fstab"


### [ Section: Hosts ]

# Configuring /etc/hosts
log_info "Configuring /etc/hosts..."
arch-chroot ${config[rootmnt]} bash -c "cat > /etc/hosts << EOF
127.0.0.1 localhost.localdomain localhost
::1 localhost.localdomain localhost
127.0.1.1 ${config[hostname]}.localdomain ${config[hostname]}
EOF" || { log_error "Failed to create /etc/hosts."; exit 1; }
log_debug "/etc/hosts configured with hostname ${config[hostname]}"

# Setting hostname
log_info "Setting hostname..."
echo "${config[hostname]}" > ${config[rootmnt]}/etc/hostname || { log_error "Failed to set hostname."; exit 1; }
log_debug "Hostname set to ${config[hostname]}"
