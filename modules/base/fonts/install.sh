#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment

# Importing fonts packages
source packages/fonts.sh


### [ Section: Fonts ]

# Fonts stage log 
log_stage "Fonts installation and configuration"

# Installing fonts
log_info "Installing fonts..."
pacman_chroot "${pkgs_fonts[@]}" || { log_error "Failed to install fonts."; exit 1; }
log_debug "Installed fonts packages"

# Disabling bitmap fonts
log_info "Disabling bitmap fonts..."
arch-chroot ${config[rootmnt]} ln -s /usr/share/fontconfig/conf.avail/70-no-bitmaps.conf /etc/fonts/conf.d/ || { log_error "Failed to disable bitmap fonts."; exit 1; }
log_debug "Bitmap fonts disabled by linking 70-no-bitmaps.conf"

# Updating font cache for both user and sudo
log_info "Updating font cache..."
arch-chroot ${config[rootmnt]} fc-cache -f -v > /dev/null 2>&1 || { log_error "Failed to update font cache."; exit 1; }
log_debug "Font cache updated for system"

log_info "Updating font cache for user..."
arch-chroot ${config[rootmnt]} sudo -u ${config[username]} fc-cache -f -v > /dev/null 2>&1 || { log_error "Failed to update font cache for user."; exit 1; }
log_debug "Font cache updated for user ${config[username]}"
