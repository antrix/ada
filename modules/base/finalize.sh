#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment

# Importing base packages
source packages/pacstrap.sh


### [ Section: Finalization ]

# Finalization stage log
log_stage "Finalization"

# Base services configuration
source modules/base/finalize_services.sh

# Base kernel parameters configuration
source modules/base/finalize_kernel.sh

# Base mkinitcpio configuration and generation
source modules/base/finalize_mkinitcpio.sh

# Base post-install script
source modules/base/finalize_postinstall.sh

# Base cleanup deployment
source modules/base/finalize_cleanup.sh