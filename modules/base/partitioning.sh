#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: Disk Operations ]

# Base partitioning stage log
log_stage "Partitioning"

# Partitioning
if [ "${config[reinstall]}" = false ]; then
    log_info "Creating partitions on ${config[target]}..."

    # Clear and create a new GPT
    sgdisk -o "${config[target]}" || { log_error "Failed to create a new GPT."; exit 1; }
    log_debug "Created new GPT on ${config[target]}"

    # Create EFI partition
    sgdisk -n 1:0:+1024MiB -t 1:ef00 -c 1:EFI "${config[target]}" || { log_error "Failed to create EFI partition."; exit 1; }
    log_debug "Created EFI partition on ${config[target]}: 1:0:+1024MiB, type ef00, name EFI"

    # Create system partition
    sgdisk -n 2:0:0 -t 2:8300 -c 2:system "${config[target]}" || { log_error "Failed to create system partition."; exit 1; }
    log_debug "Created system partition on ${config[target]}: 2:0:0, type 8300, name system"

    # Inform the operating system kernel of partition table changes
    partprobe -s "${config[target]}" || { log_error "Failed to probe partitions."; exit 1; }
    log_debug "Probed partitions on ${config[target]}"
fi

# Formatting partitions
log_info "Making File Systems..."

# Format the EFI partition with FAT32 filesystem
mkfs.vfat -F32 -n EFI /dev/disk/by-partlabel/EFI || { log_error "Failed to format EFI partition."; exit 1; }
log_debug "Formatted EFI partition as FAT32"

# Format the system partition with Btrfs filesystem
if [ "${config[reinstall]}" = false ] || ! blkid | grep -q 'system'; then
    mkfs.btrfs -f -L system /dev/disk/by-partlabel/system || { log_error "Failed to format system partition."; exit 1; }
    log_debug "Formatted system partition as Btrfs"
fi

# Mounting partitions
log_info "Mounting File Systems..."

# Mount the system partition to the root mount point
mount /dev/disk/by-partlabel/system "${config[rootmnt]}" || { log_error "Failed to mount system partition."; exit 1; }
log_debug "Mounted system partition at ${config[rootmnt]}"

# Handle subvolumes for reinstall
if [ "${config[reinstall]}" = true ]; then
    log_info "Renaming user directories in home subvolume..."
    current_date=$(date +%Y%m%d%H%M%S)
    for user_dir in "${config[rootmnt]}/home"/*; do
        if [ -d "$user_dir" ]; then
            user_name=$(basename "$user_dir")
            mv "$user_dir" "${user_dir}_${current_date}" || { log_error "Failed to rename user directory $user_dir"; exit 1; }
            log_debug "Renamed $user_dir to ${user_dir}_${current_date}"
        fi
    done

    log_info "Deleting existing subvolumes except home..."
    for subvol in $(btrfs subvolume list -o "${config[rootmnt]}" | grep -v 'home' | awk '{print $9}'); do
        log_info "Cleaning subvolume $subvol..."
        rm -rf "${config[rootmnt]}/$subvol"/* || { log_error "Failed to clean subvolume $subvol"; exit 1; }
        btrfs subvolume delete "${config[rootmnt]}/$subvol" || { log_error "Failed to delete subvolume $subvol"; exit 1; }
        log_debug "Deleted subvolume $subvol"
    done
fi

# Create required subvolumes
log_info "Creating required subvolumes..."
btrfs subvolume create "${config[rootmnt]}/root" || { log_error "Failed to create root subvolume."; exit 1; }
log_debug "Created root subvolume"

btrfs subvolume create "${config[rootmnt]}/snapshots" || { log_error "Failed to create snapshots subvolume."; exit 1; }
log_debug "Created snapshots subvolume"

# Create home subvolume if we're installing Arch first time
if [ "${config[reinstall]}" = false ]; then
    log_info "Creating home subvolume..."
    btrfs subvolume create "${config[rootmnt]}/home" || { log_error "Failed to create home subvolume."; exit 1; }
    log_debug "Created home subvolume"
fi

# Unmount and remount with correct options
log_info "Unmounting and remounting with correct options..."
umount "${config[rootmnt]}" || { log_error "Failed to unmount rootmnt."; exit 1; }
log_debug "Unmounted ${config[rootmnt]}"

mount -t btrfs -o subvol=root,defaults,x-mount.mkdir,compress=zstd,noatime LABEL=system "${config[rootmnt]}" || { log_error "Failed to mount root subvolume."; exit 1; }
log_debug "Mounted root subvolume at ${config[rootmnt]}"

mount -t btrfs -o subvol=home,defaults,x-mount.mkdir,compress=zstd,noatime LABEL=system "${config[rootmnt]}/home" || { log_error "Failed to mount home subvolume."; exit 1; }
log_debug "Mounted home subvolume at ${config[rootmnt]}/home"

mount -t btrfs -o subvol=snapshots,defaults,x-mount.mkdir,compress=zstd,noatime LABEL=system "${config[rootmnt]}/.snapshots" || { log_error "Failed to mount snapshots subvolume."; exit 1; }
log_debug "Mounted snapshots subvolume at ${config[rootmnt]}/.snapshots"

# Create and mount boot
log_info "Creating and mounting boot directory..."
mkdir -p ${config[rootmnt]}/boot || { log_error "Failed to create boot directory."; exit 1; }
log_debug "Created boot directory at ${config[rootmnt]}/boot"

mount /dev/disk/by-partlabel/EFI ${config[rootmnt]}/boot || { log_error "Failed to mount EFI partition."; exit 1; }
log_debug "Mounted EFI partition at ${config[rootmnt]}/boot"
