#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: Services ]

# Enabling services
log_info "Enabling services..."
services_enable "${services_to_enable[@]}"
log_debug "Enabled services"

log_info "Enabling user services..."
services_enable_user "${services_to_enable_user[@]}"
log_debug "Enabled user services"

# Masking services
log_info "Masking services..."
services_mask "${services_to_mask[@]}"
log_debug "Masked services"