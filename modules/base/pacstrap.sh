#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment

# Importing base packages
source packages/pacstrap.sh


### [ Section: Pacstrap ]

# Base installation stage log
log_stage "Base installation"

log_info "Updating package database in live environment..."
pacman -Syy || { log_error "Failed to update package database in live environment."; exit 1; }
log_debug "Package database updated in live environment"

# Pacstrap base packages
log_info "Pacstrapping..."
pacstrap -K ${config[rootmnt]} "${pkgs_pacstrap[@]}" || { log_error "Failed to install base packages."; exit 1; }
log_debug "Installed base packages"

# Installing and enabling services
services_to_enable+=("systemd-resolved" "systemd-timesyncd" "NetworkManager" "sshd" "avahi-daemon")
log_debug "Added services to enable: systemd-resolved, systemd-timesyncd, NetworkManager, sshd, avahi-daemon"

# Masking systemd service
services_to_mask+=("systemd-networkd")
log_debug "Added services to mask: systemd-networkd"
