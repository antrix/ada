#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: Bootloader ]

# Installing systemd-boot bootloader
log_info "Installing systemd-boot bootloader..."
arch-chroot ${config[rootmnt]} bootctl install --esp-path=/boot || { log_error "Failed to install systemd-boot."; exit 1; }
log_debug "Systemd-boot bootloader installed at /boot"

# Configuring bootloader entries
log_info "Configuring /boot/loader/entries/arch.conf..."
arch-chroot ${config[rootmnt]} bash -c "cat > /boot/loader/entries/arch.conf << EOF
title   Arch Linux
linux   /vmlinuz-linux
initrd  /intel-ucode.img
initrd  /initramfs-linux.img
options root=LABEL=system rootflags=subvol=root rw rootfstype=btrfs quiet
EOF" || { log_error "Failed to create arch.conf."; exit 1; }
log_debug "Created /boot/loader/entries/arch.conf with appropriate boot options"

# Installing and enabling service
services_to_enable+=("systemd-boot-update.service")
log_debug "Added systemd-boot-update.service to services_to_enable"

# Configuring bootloader loader.conf
log_info "Configuring /boot/loader/loader.conf..."
arch-chroot ${config[rootmnt]} bash -c "cat > /boot/loader/loader.conf << EOF
default       arch.conf
timeout       1
console-mode  max
editor        no
EOF" || { log_error "Failed to create loader.conf."; exit 1; }
log_debug "Created /boot/loader/loader.conf with default settings"
