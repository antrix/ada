#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: Post-installation Script ]

post_install_gnome=true

# Check if the desktop environment is GNOME
if [ "${config[desktop]}" != "gnome" ]; then
    log_info "Skipping GNOME post installation configuration."
    post_install_gnome=false
fi

if [ "$install_theme" = true ]; then
    # Function to create the post-install script
    log_info "Creating post-install script..."
    arch-chroot ${config[rootmnt]} sudo -u ${config[username]} bash -c "cat > /home/${config[username]}/ada_post_install.sh << 'EOF'
#!/bin/bash

# Set up en/ru keyboard layout and switching with Alt+Shift and Shift+Alt
gsettings set org.gnome.desktop.input-sources sources \"[('xkb', 'us'), ('xkb', 'ru')]\"
gsettings set org.gnome.desktop.wm.keybindings switch-input-source \"['<Alt>Shift_L', '<Shift>Alt_L']\"
gsettings set org.gnome.desktop.wm.keybindings switch-input-source-backward \"['<Shift>Alt_L', '<Alt>Shift_L']\"

# Set wallpapers
gsettings set org.gnome.desktop.background picture-uri 'file:///usr/share/backgrounds/gnome/amber-l.jxl'
gsettings set org.gnome.desktop.background picture-uri-dark 'file:///usr/share/backgrounds/gnome/amber-d.jxl'

# Enable dark mode
gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'

# Set legacy applications theme to Adw-gtk3-dark
gsettings set org.gnome.desktop.interface gtk-theme 'adw-gtk3-dark'

# Set FileChooser to sort folders before files
gsettings set org.gtk.Settings.FileChooser sort-directories-first true
gsettings set org.gtk.gtk4.Settings.FileChooser sort-directories-first true

# Set Nautilus default view to list and icon size to small
gsettings set org.gnome.nautilus.preferences default-folder-viewer 'list-view'
gsettings set org.gnome.nautilus.list-view default-zoom-level 'small'

# Set up fonts
gsettings set org.gnome.desktop.interface font-name 'Fira Sans Condensed, Condensed 11'
gsettings set org.gnome.desktop.interface document-font-name 'Fira Sans Condensed, Condensed 11'
gsettings set org.gnome.desktop.interface font-antialiasing 'rgba'

# Use machinectl shell to enter GDM session and set GDM font and antialiasing
sudo machinectl shell gdm@ /usr/bin/env gsettings set org.gnome.desktop.interface font-name 'Fira Sans Condensed, Condensed 11'
sudo machinectl shell gdm@ /usr/bin/env gsettings set org.gnome.desktop.interface font-antialiasing 'rgba'

# Remove the script itself after the first run
SCRIPT_PATH=\$(realpath \"\$0\")
rm -- \"\$SCRIPT_PATH\"

# Remove the autostart entry
AUTOSTART_ENTRY=\"\$HOME/.config/autostart/ada_post_install.desktop\"
rm -- \"\$AUTOSTART_ENTRY\"
EOF" || { log_error "Failed to create ada_post_install.sh."; exit 1; }

    # Make the script executable
    arch-chroot ${config[rootmnt]} sudo -u ${config[username]} chmod +x /home/${config[username]}/ada_post_install.sh || { log_error "Failed to make ada_post_install.sh executable."; exit 1; }

    # Create the autostart entry
    log_info "Creating autostart entry for post-install script..."
    arch-chroot ${config[rootmnt]} sudo -u ${config[username]} bash -c "mkdir -p /home/${config[username]}/.config/autostart && cat > /home/${config[username]}/.config/autostart/ada_post_install.desktop << 'EOF'
[Desktop Entry]
Type=Application
Exec=/home/${config[username]}/ada_post_install.sh
Hidden=false
NoDisplay=false
X-GNOME-Autostart-enabled=true
Name=A.D.A. Post Install Script
Comment=Run A.D.A. post-install script to set up initial configurations
DBusActivatable=true
EOF" || { log_error "Failed to create ada_post_install.desktop."; exit 1; }

    log_info "GNOME post-install script and autostart entry created successfully."
fi
