#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: ZRAM ]

# Configuring zram
log_info "Configuring zram..."
arch-chroot ${config[rootmnt]} bash -c "cat > /etc/systemd/zram-generator.conf << EOF
[zram0]
zram-size = ram / 2
compression-algorithm = zstd
EOF" || { log_error "Failed to create zram-generator.conf."; exit 1; }
log_debug "Created /etc/systemd/zram-generator.conf with zram-size = ram / 2 and compression-algorithm = zstd"

# Installing and enabling service
services_to_enable+=("systemd-zram-setup@.service")
log_debug "Added systemd-zram-setup@.service to services_to_enable"