#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: Configuration ]

# Base configuration stage log
log_stage "Base configuration"

# Base system configurations
source modules/base/config_system.sh

# Base booloader installation and configuration
source modules/base/config_bootloader.sh

# Base mkinitcpio configuration
source modules/base/config_mkinitcpio.sh

# Base users configurations
source modules/base/config_users.sh

# Base zram configurations
source modules/base/config_zram.sh

# Base polkit configurations
source modules/base/config_polkit.sh