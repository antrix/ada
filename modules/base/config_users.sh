#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: User Management ]

# Creating user account
log_info "Creating user..."

# Create a new user account
# -G wheel : Add the user to the wheel group, which allows sudo access
# -m : Create the user's home directory
arch-chroot ${config[rootmnt]} useradd -G wheel -m ${config[username]} || { log_error "Failed to create user account."; exit 1; }
log_debug "User ${config[username]} created and added to wheel group"

# Set the user password
# chpasswd -e : Read the password in encrypted format
echo "${config[username]}:${config[password]}" | arch-chroot ${config[rootmnt]} chpasswd -e || { log_error "Failed to set user password."; exit 1; }
log_debug "Password set for user ${config[username]}"

# Set the full name for the user
log_info "Setting full name for the user..."
arch-chroot ${config[rootmnt]} chfn -f "${config[username_full]}" ${config[username]} > /dev/null 2>&1 || { log_error "Failed to set full name for the user."; exit 1; }
log_debug "Full name set for user ${config[username]}: ${config[username_full]}"

# Uncomment the line for wheel group in /etc/sudoers to allow sudo without password
# NOPASSWD: Allows wheel group to use sudo without password
arch-chroot ${config[rootmnt]} sed -i -e '/^# %wheel ALL=(ALL:ALL) NOPASSWD: ALL/s/^# //' /etc/sudoers || { log_error "Failed to configure sudoers."; exit 1; }
log_debug "Configured sudoers to allow wheel group sudo access without password"

# Adding user to groups
log_info "Adding user to groups..."
arch-chroot ${config[rootmnt]} usermod -aG video,audio,storage,disk,network,input,power ${config[username]} || { log_error "Failed to add user to groups."; exit 1; }
log_debug "User ${config[username]} added to groups: video, audio, storage, disk, network, input, power"

# Setting root password
log_info "Setting root password..."
echo "root:${config[password]}" | arch-chroot ${config[rootmnt]} chpasswd -e || { log_error "Failed to set root password."; exit 1; }
log_debug "Root password set"

# Basic git configuration for the user
log_info "Configuring git for the user..."
arch-chroot ${config[rootmnt]} sudo -u ${config[username]} git config --global user.name "${config[username_full]}" || { log_error "Failed to set git user name."; exit 1; }
log_debug "Git user name set to ${config[username_full]} for user ${config[username]}"

arch-chroot ${config[rootmnt]} sudo -u ${config[username]} git config --global init.defaultBranch main || { log_error "Failed to set git default branch."; exit 1; }
log_debug "Git default branch set to main for user ${config[username]}"
