#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: Polkit Rules Configuration  ]

# Add Polkit rule to allow machinectl shell without password for wheel group
log_info "Adding Polkit rule to allow machinectl shell without password for wheel group..."

arch-chroot ${config[rootmnt]} bash -c "cat > /etc/polkit-1/rules.d/49-nopasswd-mc.rules << 'EOF'
polkit.addRule(function(action, subject) {
    if (action.id == \"org.freedesktop.machine1.shell\" &&
        subject.isInGroup(\"wheel\")) {
        return polkit.Result.YES;
    }
});
EOF" || { log_error "Failed to add Polkit rule."; exit 1; }

log_debug "Polkit rule has been added"