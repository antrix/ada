#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment

# Importing multimedia packages
source packages/multimedia.sh


### [ Section: Multimedia ]

# Multimedia stage log 
log_stage "Multimedia installation and configuration"

# Installing multimedia packages
log_info "Installing multimedia packages..."
pacman_chroot "${pkgs_multimedia[@]}" || { log_error "Failed to install multimedia packages."; exit 1; }
log_debug "Installed multimedia packages"

# Adding services to enable for user
services_to_enable_user+=("pipewire.service" "pipewire-pulse.service")
log_debug "Added user services to enable: pipewire.service, pipewire-pulse.service"
