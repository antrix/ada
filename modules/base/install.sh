#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: Partitioning ]

# Base partitioning
source modules/base/partitioning.sh


### [ Section: PAC Strapping ]

# Base pacstrap packages
source modules/base/pacstrap.sh


### [ Section: Configuration ]

# Base system configurations
source modules/base/config.sh


### [ Section: Firewall ]

# Base firewall installation and configuration
source modules/base/firewall/install.sh


### [ Section: Power/Thermal ]

# Base power/thermal installation and configuration
source modules/base/power/install.sh


### [ Section: Fonts ]

# Base fonts installation and configuration
source modules/base/fonts/install.sh


### [ Section: Display Servers ]

# Base display servers installation
source modules/base/display_servers/install.sh


### [ Section: Multimedia ]

# Base multimedia installations
source modules/base/multimedia/install.sh
