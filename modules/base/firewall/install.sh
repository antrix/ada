#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment

# Importing firewall packages
source packages/firewall.sh


### [ Section: Firewall ]

# Firewall stage log
log_stage "Firewall installation and configuration"

# Installing, enabling and configuring firewall
log_info "Installing firewall..."
pacman_chroot "${pkgs_fw[@]}" || { log_error "Failed to install firewall packages."; exit 1; }
log_debug "Installed firewall packages"

log_info "Configuring firewall..."
arch-chroot ${config[rootmnt]} ufw enable || { log_error "Failed to enable ufw."; exit 1; }
log_debug "UFW enabled"

# Installing and enabling service
services_to_enable+=("ufw")
log_debug "Added ufw to services_to_enable"

log_info "Setting default firewall policies..."
arch-chroot ${config[rootmnt]} ufw default deny incoming || { log_error "Failed to set default policy for incoming traffic."; exit 1; }
log_debug "Default policy for incoming traffic set to deny"

arch-chroot ${config[rootmnt]} ufw default allow outgoing || { log_error "Failed to set default policy for outgoing traffic."; exit 1; }
log_debug "Default policy for outgoing traffic set to allow"
