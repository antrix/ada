#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment

# Importing display servers packages
source packages/display_servers.sh


### [ Section: Display Servers and Drivers ]

# Display servers stage log 
log_stage "Display servers installation and configuration"

# Installing display servers
log_info "Installing display servers..."
pacman_chroot "${pkgs_display_server[@]}" || { log_error "Failed to install display servers packages."; exit 1; }
log_debug "Installed display servers packages"
