#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: Host Info ]

# A.D.A. info stage log
log_stage "A.D.A. (Arch Deployment Assistant)"

# Check if the target device is valid
if ! device_valid "${config[target]}"; then
    log_error "The target device is either invalid or blacklisted. Please specify a valid target device with --target."
    exit 1
fi

# Get disk information
disk_model=$(device_get_model_name "${config[target]}")
disk_serial=$(device_get_serial_number "${config[target]}")
disk_manufacturer=$(device_get_manufacturer "${config[target]}")

# Show the script parameters
log_separator

log_param "Mode" "$( [ "${config[reinstall]}" = true ] && echo "Reinstall" || echo "Clean Install")"
log_param "Target Disk" "${config[target]} (Model: $disk_model, Serial: $disk_serial, Manufacturer: $disk_manufacturer)"
log_param "Mount Dir" "${config[rootmnt]}"
log_param "Stage Clear" "${config[stage_clear]}"

log_separator

log_param "Locale" "${config[locale]}"
log_param "Keymap" "${config[keymap]}"

log_separator

log_param "Country" "${config[country]}"
log_param "Timezone" "${config[timezone]}"

log_separator

log_param "Desktop" "${config[desktop]}"
log_separator

log_param "Hostname" "${config[hostname]}"
log_param "Username" "${config[username]}"
log_param "Username Full" "${config[username_full]}"
log_param "Password Hash" "${config[password]}"

log_separator

log_param "NVIDIA Beta Drivers" "${config[drivers_nvidia_beta]}"
log_separator

log_param "Reconfigure Mirrors" "${config[mirrors_update]}"

log_separator

log_param "Pacman Noconfirm" "${config[pacman_noconfirm]}"
log_param "Pacman Needed" "${config[pacman_needed]}"
log_param "Pacman Disable Timeout" "${config[pacman_disable_dl_timeout]}"
log_param "Pacman Ask" "${config[pacman_ask]}"

log_separator

# Host installation prompt
while true; do
    prompt_choice "Do you want to continue with the installation?" "${BRIGHT_GREEN}yes${NC}" "${BRIGHT_RED}no${NC}"
    choice=$CHOICE_INDEX

    if [[ $choice -eq 0 ]]; then
        # Continue with the installation...
        break
    elif [[ $choice -eq 1 ]]; then
        # Exit...
        exit 1
    fi
done