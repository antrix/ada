#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: Host ]

# Host pre-installation info
source modules/host/host_info.sh

# Host pre-installation Checks
source modules/host/host_checks.sh

# Host pre-installation configurations
source modules/host/host_config.sh