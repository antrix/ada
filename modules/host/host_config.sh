#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment


### [ Section: Host Configuration ]

# Update system clock
timedatectl set-ntp true


### [ Section: Pacman ]

# Configuring pacman in live environment
log_info "Configuring pacman in live environment..."
if grep -q '^#Color$' /etc/pacman.conf 2>/dev/null; then
    sed -i 's/^#Color$/Color/' /etc/pacman.conf || { log_error "Failed to enable pacman color in live environment."; exit 1; }
    log_debug "Pacman color enabled in live environment"
else
    log_debug "Pacman color already enabled in live environment"
fi

if grep -q '^#ParallelDownloads = 5' /etc/pacman.conf 2>/dev/null; then
    sed -i 's/^#ParallelDownloads = 5/ParallelDownloads = 50/' /etc/pacman.conf || { log_error "Failed to configure pacman parallel downloads in live environment."; exit 1; }
    log_debug "Pacman parallel downloads set to 50 in live environment"
else
    log_debug "Pacman parallel downloads already set in live environment"
fi


### [ Section: Repositories ]

# Adding A.L.I.C.E. repository to live ISO environment
log_info "Checking for A.L.I.C.E. repository in live environment..."
if ! grep -q "\[alice\]" /etc/pacman.d/alice.conf 2>/dev/null; then
    echo -e "[alice]\nSigLevel = Optional TrustAll\nServer = http://94.16.114.182" | tee /etc/pacman.d/alice.conf > /dev/null || { log_error "Failed to create /etc/pacman.d/alice.conf in live environment."; exit 1; }
    log_debug "A.L.I.C.E. repository configuration file created in live environment"
else
    log_debug "A.L.I.C.E. repository configuration file already exists in live environment"
fi

# Adding include for A.L.I.C.E. repository in pacman.conf in live environment
if ! grep -q "Include = /etc/pacman.d/alice.conf" /etc/pacman.conf 2>/dev/null; then
    echo -e "\nInclude = /etc/pacman.d/alice.conf" | tee -a /etc/pacman.conf > /dev/null || { log_error "Failed to add include for A.L.I.C.E. repository to pacman.conf in live environment."; exit 1; }
    log_debug "Include for A.L.I.C.E. repository added to pacman.conf in live environment"
else
    log_debug "Include for A.L.I.C.E. repository already exists in pacman.conf in live environment"
fi


### [ Section: Mirrors ]

if [ "${config[mirrors_update]}" = true ]; then
    # Configuring mirrors
    log_info "Configuring mirrors..."
    reflector --country "${config[country]}" --age 12 --protocol http,https --sort rate --save /etc/pacman.d/mirrorlist || { log_error "Failed to configure mirrors."; exit 1; }
    
    # Extracting and logging the mirrors
    mirrors=$(grep '^Server =' /etc/pacman.d/mirrorlist | awk '{print $3}')
    
    if [ -n "$mirrors" ]; then
        log_debug "Mirrors used for configuration:"
        echo "$mirrors" | while read -r mirror; do
            log_debug "$mirror"
        done
    else
        log_debug "No mirrors found."
    fi
fi

# Update package database
log_info "Updating package database..."
pacman -Syy --noconfirm || { log_error "Failed to update package database."; exit 1; }