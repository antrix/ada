#!/bin/bash

# Check if the script is being run from ada.sh
check_ada_environment

# Importing required packages
source packages/required.sh

### [ Section: Pre-installation Checks ]

# Performing initial setup checks stage log
log_stage "Performing initial setup checks"

# Check if we're root
log_info "Checking root..."
if [[ "$UID" -ne 0 ]]; then
    log_error "This script needs to be run as root!"
    exit 1
fi
log_debug "Current UID: $UID"

# Check internet connection
log_info "Checking network connection..."
if ! ping -c 1 google.com &> /dev/null; then
    log_error "No network connection. Please ensure you are connected to the internet."
    exit 1
fi

network_device=$(ip route get 1.1.1.1 | awk '{print $5; exit}')
network_ip_address=$(ip -o -f inet addr show $network_device | awk '{print $4}')
network_type=$(iw dev $network_device info &> /dev/null && echo "Wireless" || echo "Wired")

log_debug "Network device: $network_device, IP address: $network_ip_address, Network type: $network_type"

# Check if required packages are installed
log_info "Checking required packages..."
for pkg in "${pkgs_required[@]}"; do
    if ! pacman -Q $pkg &> /dev/null; then
        log_warning "Required package $pkg is not installed. Installing..."
        if ! pacman -S --noconfirm $pkg; then
            log_error "Failed to install required package $pkg."
            exit 1
        else
            log_info "Successfully installed package $pkg."
        fi
    fi
    log_debug "Package $pkg is installed."
done

# Check if rootmnt exists, create if not
log_info "Checking rootmnt..."
if [ ! -d "${config[rootmnt]}" ]; then
    mkdir -p "${config[rootmnt]}" || { log_error "Failed to create rootmnt directory."; exit 1; }
    log_debug "Created rootmnt directory: ${config[rootmnt]}"
else
    # Check if something is mounted and unmount it
    if mountpoint -q "${config[rootmnt]}"; then
        log_info "Unmounting existing mounts in rootmnt..."
        umount -R "${config[rootmnt]}" || { log_error "Failed to unmount rootmnt recursively."; exit 1; }
        log_debug "Unmounted existing mounts in rootmnt: ${config[rootmnt]}"
    fi
fi
log_debug "Mount point: ${config[rootmnt]}"

# Check if DE install script exists
log_info "Checking DE script..."
de_install_script="modules/desktop/${config[desktop]}/install.sh"
if [ ! -f "$de_install_script" ]; then
    log_error "The installation script for the selected DE (${config[desktop]}) does not exist: $de_install_script"
    exit 1
fi
log_debug "DE (${config[desktop]}) install script exists: $de_install_script"

# Check if the system is in a VM and its type
log_info "Checking machine type..."
if vm_type=$(systemd-detect-virt); then
    if [ -n "$vm_type" ]; then
        log_debug "Detected VM type: $vm_type"
    else
        log_debug "Detected physical machine."
    fi
else
    log_error "Failed to detect machine type."
fi