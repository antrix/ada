#!/bin/bash

# Applications packages
pkgs_apps=(
    # Communication tools
    discord                         # Discord client
    telegram-desktop                # Telegram client
    steam                           # Steam, Praise GabeN!

    # System information
    fastfetch                       # System information tool

    # Compatibility layers
    mangohud                        # Vulkan and OpenGL overlay for monitoring FPS and system performance
    wine                            # Windows compatibility layer
    winetricks                      # Helper script for Wine
    protontricks-git                # [CUSTOM] Protontricks for Steam Play

    # TODO: Move it to the better place
    lenovolegionlinux-dkms-git      # [CUSTOM] Lenovo Legion drivers
)
