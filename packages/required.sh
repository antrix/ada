#!/bin/bash

# Required packages
pkgs_required=(
    "reflector"                     # Script to retrieve and filter the latest Pacman mirror list.
    "btrfs-progs"                   # Btrfs filesystem utilities
)