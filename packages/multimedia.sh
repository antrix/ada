#!/bin/bash

# Multimedia packages
pkgs_multimedia=(
    # Pipewire and related packages
    gst-plugin-pipewire             # GStreamer plugin for Pipewire
    libpulse                        # PulseAudio client libraries
    pipewire                        # Low-latency multimedia framework
    pipewire-alsa                   # ALSA configuration for Pipewire
    pipewire-jack                   # JACK support for Pipewire
    pipewire-pulse                  # PulseAudio support for Pipewire
    wireplumber                     # Session manager for Pipewire
)