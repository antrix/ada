#!/bin/bash

# Display server packages (Xorg and Wayland)
pkgs_display_server=(
    mesa                            # Mesa 3D Graphics Library
    xorg-server                     # Xorg X server
    xorg-xinit                      # X.Org initialisation program
    xorg-xwayland                   # XWayland allows X11 applications to run on Wayland
    wayland                         # Wayland display server protocol
    wayland-protocols               # Wayland protocols
)