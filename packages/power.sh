#!/bin/bash

# Power and Thermal Management packages
pkgs_power_thermal=(
    lm_sensors                      # Hardware health monitoring
    power-profiles-daemon           # Power profiles daemon
    thermald                        # Thermal management daemon
)
