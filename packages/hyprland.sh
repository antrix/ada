#!/bin/bash

# Hyprland Desktop Environment
pkgs_hyprland=(
    # Core system components
    hyprland                        # Hyprland window manager
    sddm                            # Simple Desktop Display Manager
    sddm-kcm                        # Configuration module for SDDM

    # Console applications
    kitty                           # Cross-platform terminal
    neovim                          # Advanced text editor
    ranger                          # Console file manager with image previews

    # Browser
    qutebrowser                     # Minimalist keyboard-driven browser

    # Hyprland ecosystem utilities
    hyprpaper                       # Wallpaper manager for Hyprland
    hyprpicker                      # [CUSTOM] Color picker tool for Hyprland
    hypridle                        # Idle management tool for Hyprland
    hyprlock                        # Screen locker for Hyprland
    hyprcursor                      # Cursor management tool for Hyprland
    xdg-desktop-portal-hyprland     # Desktop portal for Hyprland

    # Additional utilities for Hyprland
    wofi                            # Launcher
    waybar                          # Status bar
    walker                          # Extensible and fast wayland application runner

    # QT utilities and Wayland support
    qt5ct                           # QT5 Configuration Tool
    qt6ct                           # QT6 Configuration Tool
    kvantum-qt5                     # QT5 theme engine
    kvantum                         # QT6 theme engine
    qt5-wayland                     # QT5 Wayland integration
    qt6-wayland                     # QT6 Wayland integration

    # Authentication agent
    polkit-kde-agent                # KDE authentication agent
)
