#!/bin/bash

# GNOME Desktop Environment
pkgs_plasma=(
    # Core system components
    sddm                            # QML based X11 and Wayland display manager
    plasma                          # KDE Plasma Desktop

    egl-wayland
    xwaylandvideobridge
    konsole
    kwrite
    dolphin
    ark
)
