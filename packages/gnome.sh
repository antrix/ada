#!/bin/bash

# GNOME Desktop Environment
pkgs_gnome=(
    # Core system components
    gdm                             # GNOME Display Manager
    gnome-shell                     # Next generation desktop shell
    gnome-session                   # The GNOME Session Handler
    gnome-control-center            # GNOME's main interface to configure various aspects of the desktop
    gnome-settings-daemon           # GNOME Settings Daemon
    gnome-keyring                   # Stores passwords and encryption keys
    gnome-backgrounds               # Background images and data for GNOME
    gnome-browser-connector         # Browser integration with GNOME Shell
    gnome-tweaks                    # GNOME Tweaks utility
    dconf-editor                    # GSettings editor for GNOME
    plymouth                        # Provides a graphical boot

    # Applications and Utilities
    gnome-calculator                # GNOME Scientific calculator
    gnome-calendar                  # Simple and beautiful calendar application
    gnome-characters                # A character map application
    gnome-clocks                    # Clocks applications for GNOME
    gnome-color-manager             # GNOME Color Profile Tools
    gnome-connections               # Remote desktop client for the GNOME desktop environment
    gnome-console                   # A simple user-friendly terminal emulator for the GNOME desktop
    gnome-contacts                  # Contacts Manager for GNOME
    gnome-disk-utility              # Disk Management Utility for GNOME
    gnome-font-viewer               # A font viewer utility for GNOME
    gnome-logs                      # A log viewer for the systemd journal
    gnome-maps                      # A simple GNOME 3 maps application
    gnome-music                     # Music player and management application
    gnome-remote-desktop            # GNOME Remote Desktop server
    gnome-system-monitor            # View current processes and monitor system state
    gnome-text-editor               # A simple text editor for the GNOME desktop
    gnome-tour                      # Guided tour and greeter for GNOME
    gnome-user-docs                 # User documentation for GNOME
    gnome-user-share                # Easy to use user-level file sharing for GNOME
    gnome-weather                   # Access current weather conditions and forecasts

    baobab                          # A graphical directory tree analyzer
    evince                          # Document viewer
    file-roller                     # File archiver
    fragments                       # BitTorrent client
    loupe                           # A simple image viewer for GNOME
    nautilus                        # Default file manager for GNOME
    snapshot                        # Take pictures and videos
    sushi                           # A quick previewer for Nautilus
    tecla                           # Keyboard layout viewer
    totem                           # Movie player for the GNOME desktop

    # Virtualization
    gnome-boxes                     # Virtualization and VM management
    spice-vdagent                   # Spice agent for improved host-guest integration and features in gnome-boxes

    # Additional Utilities and Features
    grilo-plugins                   # A collection of plugins for the Grilo framework
    rygel                           # UPnP AV MediaServer and MediaRenderer
    tracker3-miners                 # Filesystem indexer and metadata extractor
    xdg-desktop-portal-gnome        # A backend implementation for xdg-desktop-portal for the GNOME desktop environment
    xdg-user-dirs-gtk               # Creates user dirs and asks to relocalize them
    yelp                            # Get help with GNOME

    # Virtual Filesystem Implementations
    gvfs                            # Virtual filesystem implementation for GIO
    gvfs-afc                        # AFC backend (Apple mobile devices)
    gvfs-dnssd                      # DNS-SD and WebDAV backend (macOS file sharing)
    gvfs-goa                        # Gnome Online Accounts backend (e.g. OwnCloud)
    gvfs-google                     # Google Drive backend
    gvfs-gphoto2                    # gphoto2 backend (PTP camera, MTP media player)
    gvfs-mtp                        # MTP backend (Android, media player)
    gvfs-nfs                        # NFS backend
    gvfs-onedrive                   # Microsoft OneDrive backend
    gvfs-smb                        # SMB/CIFS backend (Windows file sharing)
    gvfs-wsdd                       # Web Services Dynamic Discovery backend (Windows discovery)

    # Various Utilities
    adwsteamgtk                     # [CUSTOM] Adwaita theme for Steam
    adw-gtk3-git                    # [CUSTOM] Adwaita theme for GTK3
    mousam                          # [CUSTOM] Weather conditions and forcasts
    firefox-gnome-theme             # [CUSTOM] irefox GNOME Adwaita theme
    gdm-settings                    # [CUSTOM] Login screen customization

    # Internet
    firefox                         # Web browser
)
