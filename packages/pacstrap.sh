#!/bin/bash

# Packages to pacstrap
pkgs_pacstrap=(
    ### Core packages
    base                            # Basic Arch Linux base system
    base-devel                      # Essential development tools
    intel-ucode                     # Microcode update for Intel CPUs
    linux                           # Linux kernel and modules
    linux-firmware                  # Firmware for Linux kernel
    linux-headers                   # Headers and scripts for building modules for the Linux kernel
    sudo                            # Allows a permitted user to execute a command as the superuser
    util-linux                      # Miscellaneous system utilities

    ### Networking
    git                             # Version control system
    iwd                             # iNet wireless daemon for managing WiFi
    networkmanager                  # Network connection manager
    openssh                         # SSH connectivity tools
    wireless_tools                  # Tools for managing wireless networks
    wpa_supplicant                  # WPA supplicant for wireless networks

    ### Filesystem tools
    e2fsprogs                       # Ext2/3/4 filesystem utilities
    dosfstools                      # Utilities for FAT filesystems
    btrfs-progs                     # Btrfs filesystem utilities

    ### Text editors
    nano                            # Simple, easy-to-use text editor
    nano-syntax-highlighting        # Syntax highlighting for nano text editor

    ### System utilities
    bash-completion                 # Programmable completion for the bash shell
    htop                            # A monitor of system resources
    man-db                          # Utility for reading man pages
    man-pages                       # Linux man pages
    mc                              # Midnight Commander, a visual file manager
    smartmontools                   # S.M.A.R.T. monitoring tools

    ### Additional utilities
    curl                            # Command line tool and library for transferring data with URLs
    rsync                           # Utility for synchronizing files and directories
    wget                            # Network downloader
    zram-generator                  # ZRAM generator for swap compression

    ### User directories
    xdg-utils                       # Basic utilities for managing XDG environments

    ### Firmware
    mkinitcpio-firmware             # [CUSTOM] Firmware for mkinitcpio
)