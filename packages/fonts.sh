#!/bin/bash

# Font packages
pkgs_fonts=(
    # Core
    fontconfig                          # Library for configuring and customizing font access
    freetype2                           # Software library to render fonts

    # Noto fonts
    noto-fonts                          # Google's Noto fonts for wide Unicode coverage
    noto-fonts-cjk                      # Google's Noto CJK fonts for Chinese, Japanese, and Korean
    noto-fonts-emoji                    # Google's Noto Emoji fonts
    noto-fonts-extra                    # Google's Noto fonts for additional languages and symbols

    # Additional useful fonts
    ttf-dejavu                          # DejaVu fonts, enhanced Bitstream Vera
    ttf-fira-code                       # Fira Code, monospaced font with programming ligatures
    ttf-fira-sans                       # Fira Sans, sans-serif font
    ttf-font-awesome                    # Iconic font for web and UI design
    ttf-liberation                      # Liberation fonts, replacements for Arial, Times New Roman, and Courier New
    ttf-opensans                        # Open Sans font
    ttf-roboto                          # Roboto font, used in Android
    ttf-ubuntu-font-family              # Ubuntu Font Family, includes Ubuntu Mono

    # Adobe fonts
    adobe-source-code-pro-fonts         # High quality monospace font for Latin characters
    adobe-source-han-sans-otc-fonts     # High quality sans-serif font for CJK characters
    adobe-source-han-serif-otc-fonts    # High quality serif font for CJK characters
    adobe-source-sans-fonts             # High quality sans-serif font for Latin characters
    adobe-source-serif-fonts            # High quality serif font for Latin characters

    # MS Fonts
    ttf-ms-win11-auto                   # [CUSTOM] Microsoft Windows 11 fonts
)
