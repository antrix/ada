#!/bin/bash

# Firewall
pkgs_fw=(
    # UFW & iptables-nft
    ufw                             # Uncomplicated Firewall
    iptables-nft                    # Modern iptables replacement
)