#!/bin/bash

# Set environment variable to indicate the script is being run from ada.sh
export ADA_ENVIRONMENT=1

# Exit immediately if a command exits with a non-zero status
set -e

### [ Section: Preparation ]

# Initializing variables, configs, packages and utilities
source config/colors.sh
source config/config.sh
source config/variables.sh

# Initializing utils
source utils/common.sh
source utils/logger.sh
source utils/kernel.sh
source utils/mkinitcpio.sh
source utils/pacman.sh
source utils/services.sh


### [ Section: Host ]

# Host pre-installation setup
source modules/host/host.sh


### [ Section: Base ]

# Base installation
source modules/base/install.sh


### [ Section: Drivers ]

# Drivers installation
source modules/drivers/install.sh


### [ Section: Desktop Environment ]

# Desktop Environment installation
source modules/desktop/install.sh


### [ Section: Extras ]

# Extras installation
source modules/extras/install.sh


### [ Section: Finalization ]

# Finalize installation
source modules/base/finalize.sh