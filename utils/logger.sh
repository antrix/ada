#!/bin/bash

# Function to log messages
log_info() {
    echo -e "${CYAN}$1${NC}"
    sleep 0.5
}

log_warning() {
    echo -e "${MAGENTA}$1${NC}"
    sleep 1
}

log_error() {
    echo -e "${RED}$1${NC}" >&2
    sleep 2
}

log_debug() {
    echo -e "${BRIGHT_YELLOW}::${NC} $1${NC}"
    sleep 0.5
}

log_param() {
    printf "${BRIGHT_GREEN}%-25s${NC} ${BRIGHT_YELLOW}%s${NC}\n" "$1:" "$2"
}

log_stage() {
    clear
    
    local term_width
    local msg="$1"
    local msg_length=${#msg}
    local padding
    local header

    term_width=$(tput cols)
    padding=$(( (term_width - msg_length - 4) / 2 )) # 4 characters for brackets and spaces

    # Form the line with background and centered text
    header="${BACKGROUND_CYAN}${FOREGROUND_BLACK}$(printf "%*s[ %s ]%*s" $padding "" "$msg" $padding "")${NC}\n"

    echo -e "${header}"
}

log_separator() {
    echo ""
}