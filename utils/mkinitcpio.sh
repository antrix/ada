#!/bin/bash

# Function to add parameters to mkinitcpio arrays
add_mkinitcpio_params() {
    local array_name=$1
    shift
    local elements=("$@")
    
    # Check if the array is empty
    if [ ${#elements[@]} -eq 0 ]; then
        return
    fi
    
    local current_elements=$(arch-chroot ${config[rootmnt]} grep "^$array_name=" /etc/mkinitcpio.conf | cut -d'(' -f2 | cut -d')' -f1)
    for element in "${elements[@]}"; do
        if [[ ! " ${current_elements[@]} " =~ " ${element} " ]]; then
            current_elements+=" $element"
        fi
    done
    arch-chroot ${config[rootmnt]} sed -i "s/^$array_name=.*/$array_name=($current_elements)/" /etc/mkinitcpio.conf || { log_error "Failed to update $array_name in mkinitcpio.conf."; exit 1; }
    log_debug "Updated $array_name in mkinitcpio.conf with elements: ${elements[*]}"
}

# Function to add hooks to mkinitcpio
add_mkinitcpio_hooks() {
    local hooks=("$@")
    add_mkinitcpio_params "HOOKS" "${hooks[@]}"
}

# Function to add modules to mkinitcpio
add_mkinitcpio_modules() {
    local modules=("$@")
    add_mkinitcpio_params "MODULES" "${modules[@]}"
}

# Function to add binaries to mkinitcpio
add_mkinitcpio_binaries() {
    local binaries=("$@")
    add_mkinitcpio_params "BINARIES" "${binaries[@]}"
}