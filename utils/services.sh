#!/bin/bash

# Function to enable services
services_enable() {
    local services=("$@")
    
    # Check if the array is empty
    if [ ${#services[@]} -eq 0 ]; then
        return
    fi
    
    for service in "${services[@]}"; do
        arch-chroot ${config[rootmnt]} systemctl enable "$service" || { log_error "Failed to enable $service."; exit 1; }
        log_debug "Enabled service: $service"
    done
}

# Function to enable user services
services_enable_user() {
    local services=("$@")
    
    # Check if the array is empty
    if [ ${#services[@]} -eq 0 ]; then
        return
    fi
    
    for service in "${services[@]}"; do
        arch-chroot ${config[rootmnt]} sudo -u ${config[username]} systemctl --user enable "$service" || { log_error "Failed to enable $service for user."; exit 1; }
        log_debug "Enabled service: $service for user ${config[username]}"
    done
}

# Function to mask services
services_mask() {
    local services=("$@")
    
    # Check if the array is empty
    if [ ${#services[@]} -eq 0 ]; then
        return
    fi
    
    for service in "${services[@]}"; do
        arch-chroot ${config[rootmnt]} systemctl mask "$service" || { log_error "Failed to mask $service."; exit 1; }
        log_debug "Masked service: $service"
    done
}
