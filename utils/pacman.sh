#!/bin/bash

# Function to install packages in chroot environment
pacman_chroot() {
    local packages=("$@")
    local pacman_options=()

    [[ "${config[pacman_noconfirm]}" == true ]] && pacman_options+=("--noconfirm")
    [[ "${config[pacman_needed]}" == true ]] && pacman_options+=("--needed")
    [[ "${config[pacman_disable_dl_timeout]}" == true ]] && pacman_options+=("--disable-download-timeout")
    [[ "${config[pacman_ask]}" == true ]] && pacman_options+=("--ask=4")

    arch-chroot ${config[rootmnt]} pacman -S "${pacman_options[@]}" "${packages[@]}"
}