#!/bin/bash

# A.D.A. environment check
check_ada_environment() {
    if [ -z "$ADA_ENVIRONMENT" ]; then
        echo "This script must be run from ada.sh"
        exit 1
    fi
}

# Function to get the serial number of a target device
# Returns the serial number or "Unknown" if not found
device_get_serial_number() {
    local target_device=$1
    local serial_number
    serial_number=$(lsblk -dno SERIAL "$target_device")
    echo "${serial_number:-Unknown}"
}

# Function to get the model name of a target device
# Returns the model name or "Unknown" if not found
device_get_model_name() {
    local target_device=$1
    local model_name
    model_name=$(lsblk -dno MODEL "$target_device")
    echo "${model_name:-Unknown}"
}

# Function to get the manufacturer of a target device
# Returns the manufacturer or "Unknown" if not found
device_get_manufacturer() {
    local target_device=$1
    local manufacturer
    manufacturer=$(smartctl -i "$target_device" | grep 'Model Family' | awk -F: '{print $2}' | xargs)
    echo "${manufacturer:-Unknown}"
}

# Function to check if the device exists
# Returns true if the device exists, false otherwise
device_exists() {
    local target_device=$1
    [[ -b "$target_device" ]]
}

# Function to check if the device is valid
# Returns true if the device exists and is not blacklisted, false otherwise
device_valid() {
    local target_device=$1
    local target_serial

    # Check if the device exists
    if ! device_exists "$target_device"; then
        return 1
    fi

    # Get the serial number of the target device
    target_serial=$(device_get_serial_number "$target_device")

    # Check if the target device is in the blacklist
    if storage_check_blacklist "$target_serial" "${storage_blacklisted[@]}"; then
        return 1
    fi

    return 0
}

# Function to check if the target device is blacklisted
# Takes the serial number and an array of blacklisted serials as arguments
# Returns 0 if the device is blacklisted, 1 otherwise
storage_check_blacklist() {
    local target_serial=$1
    shift
    local blacklisted_serials=("$@")

    for serial in "${blacklisted_serials[@]}"; do
        if [[ "$target_serial" == "$serial" ]]; then
            return 0
        fi
    done
    return 1
}

# Function to find the first available device from the priority list
# Takes an array of priority serials as argument
# Returns the device path if found, "None" otherwise
storage_find_priority_device() {
    local priority_serials=("$@")

    for priority_serial in "${priority_serials[@]}"; do
        for device in /dev/nvme* /dev/sd*; do
            [[ -e "$device" ]] || continue
            device_serial=$(device_get_serial_number "$device")
            if [[ "$device_serial" == "$priority_serial" ]]; then
                echo "$device"
                return 0
            fi
        done
    done
    echo "None"
    return 1
}

# Function to prompt for choices and return the selected option
prompt_choice() {
    local color_message=$BRIGHT_MAGENTA
    local color_brackets=$BRIGHT_WHITE
    local color_slashes=$BRIGHT_CYAN

    local message=$1
    shift
    local choices=("$@")
    local choice

    local options=""
    for i in "${!choices[@]}"; do
        if [[ $i -gt 0 ]]; then
            options+="$color_slashes/$NC"
        fi
        options+="${choices[i]}"
    done

    while true; do
        echo -en "${color_message}${message} ${color_brackets}(${options}${color_brackets}): ${NC}"
        read -r choice

        for i in "${!choices[@]}"; do
            clean_choice=$(echo -e "${choices[i]}" | sed 's/\x1b\[[0-9;]*m//g')  # Remove color codes for comparison
            if [[ "$choice" == "$clean_choice" ]]; then
                CHOICE_INDEX=$i
                return
            fi
        done

        echo -e "${BRIGHT_RED}Invalid response. Please enter one of the following: ${options}${NC}"
    done
}