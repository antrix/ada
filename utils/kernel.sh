#!/bin/bash

# Function to add kernel parameters
add_kernel_params() {
    local params=("$@")
    
    # Check if the array is empty
    if [ ${#params[@]} -eq 0 ]; then
        return
    fi
    
    options_line=$(grep -m 1 "^options" ${config[rootmnt]}/boot/loader/entries/arch.conf)
    for param in "${params[@]}"; do
        if [[ ! "$options_line" =~ "$param" ]]; then
            options_line+=" $param"
        fi
    done
    sed -i "s|^options.*|$options_line|" ${config[rootmnt]}/boot/loader/entries/arch.conf || { log_error "Failed to update kernel parameters."; exit 1; }
    log_debug "Updated kernel parameters in arch.conf with: ${params[*]}"
}
